import "./Sidebar.css";

import React from "react";
import {
  withRouter,
  Link,
  RouteComponentProps,
  useLocation,
} from "react-router-dom";
import { matchPath } from "react-router";

interface IEntry {
  path: string;
  label: string;
  icon: React.ReactNode;
}

interface ISidebarProps {
  entries: IEntry[];
}

const Sidebar: React.FC<ISidebarProps & RouteComponentProps> = ({
  entries,
}) => {
  const location = useLocation();

  const isRouteSelected = (route: string) => {
    return !!matchPath(location.pathname, {
      path: route,
      exact: true,
      strict: false,
    });
  };

  return (
    <React.Fragment>
      <div className="sidebar-logo">
        <h1>SenCity</h1>
      </div>
      <nav className="sidebar">
        <ul>
          {entries.map((e) => {
            const className = isRouteSelected(e.path) ? "selected-route" : "";

            const routeWithoutParams =
              e.path.indexOf(":") !== -1
                ? e.path.substring(0, e.path.indexOf(":"))
                : e.path;

            return (
              <li key={routeWithoutParams} className={className}>
                <Link to={routeWithoutParams}>
                  {e.icon}
                  <p>{e.label}</p>
                </Link>
              </li>
            );
          })}
        </ul>
      </nav>
    </React.Fragment>
  );
};

export default withRouter(Sidebar);
