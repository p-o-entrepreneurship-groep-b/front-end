import React, { FunctionComponent } from "react";

import MapGL, { ViewportChangeHandler } from "react-map-gl";
import { PointerEvent } from "react-map-gl";

const MAPBOX_TOKEN =
  "pk.eyJ1IjoibHVpZ2lpaWlpaWkiLCJhIjoiY2s3dzQ4N2s0MDBmODNscXZ0MWRicjVzYyJ9.22f8kmcROhG5iUmhwJ9MHg";

const DEFAULT_LONGITUDE = 4.701555;
const DEFAULT_LATITUDE = 50.878876;
const DEFAULT_ZOOM = 12;

interface Viewport {
  longitude: number;
  latitude: number;
  zoom: number;
}

interface HubMapProps {
  height: number;
  width?: number;
  longitude?: number;
  latitude?: number;
  zoom?: number;
  fixed?: boolean;
  children?: React.ReactNode;
  onClick?: (event: PointerEvent) => void;
}

const HubMap: FunctionComponent<HubMapProps> = ({
  height,
  width,
  children,
  onClick,
  longitude = DEFAULT_LONGITUDE,
  latitude = DEFAULT_LATITUDE,
  zoom = DEFAULT_ZOOM,
  fixed = false,
}) => {
  const [viewPort, setViewPort] = React.useState<Viewport>({
    longitude: longitude,
    latitude: latitude,
    zoom: zoom,
  });

  if (fixed && viewPort.longitude !== longitude) {
    setViewPort({ ...viewPort, longitude, latitude, zoom });
  }

  const _onViewportChange: ViewportChangeHandler = (viewport) => {
    if (!fixed) {
      setViewPort({ ...viewport });
    } else {
      setViewPort({ ...viewPort, longitude, latitude, zoom });
    }
  };

  return (
    <MapGL
      {...viewPort}
      width={width === undefined ? "100%" : width}
      height={height}
      onViewportChange={_onViewportChange}
      mapStyle="mapbox://styles/luigiiiiiii/ck7w4b3q200781jqrruknh2ui"
      mapboxApiAccessToken={MAPBOX_TOKEN}
      onClick={onClick}
    >
      {children}
    </MapGL>
  );
};

export default HubMap;
