import React from "react";

import "../../App.css";
import { useForm } from "react-hook-form";

import { useStoreState } from "../../Store";

import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { Module } from "../../Api";
import Modal from "../Modal/Modal";

const GUID_PATTERN = /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i;

interface CreateModuleProps {
  show: boolean;
  handleClose: Function;
}

const CreateModule: React.FC<CreateModuleProps> = ({ show, handleClose }) => {
  const { register, handleSubmit, errors, reset } = useForm<Module>({
    mode: "onBlur",
  });

  const modules = useStoreState((state) => state.module.modules);

  const isGuidFree = (guid: string) =>
    modules.filter((m) => m.guid === guid).length === 0;

  const onSubmit = (data: Module) => {
    console.log(data);
  };

  return (
    <Modal show={show} modalClassname="modal-outer-form">
      <Paper className="modal-form-surface">
        <form onSubmit={handleSubmit(onSubmit)}>
          <h2>Create module</h2>
          <div className="modal-form-item">
            <TextField
              name="name"
              label="Name"
              fullWidth={true}
              variant="filled"
              type="text"
              inputRef={register({
                required: "You must enter a name",
                minLength: {
                  value: 5,
                  message: "The name must be at least 5 characters long",
                },
              })}
              error={errors.name !== undefined}
              helperText={errors.name && errors.name.message}
            />
          </div>
          <div className="modal-form-item">
            <TextField
              name="guid"
              label="Guid"
              fullWidth={true}
              variant="filled"
              type="text"
              inputRef={register({
                required: "You must enter a guid",
                pattern: {
                  value: GUID_PATTERN,
                  message: "You must enter a valid guid",
                },
                validate: (value) =>
                  isGuidFree(value) ||
                  "A module with the given guid already exists",
              })}
              error={errors.guid !== undefined}
              helperText={errors.guid && errors.guid.message}
            />
          </div>
          <div className="modal-form-items modal-form-controls">
            <Button
              variant="contained"
              color="secondary"
              size="large"
              onClick={() => {
                reset();
                handleClose();
              }}
            >
              Cancel
            </Button>
            <Button
              variant="contained"
              color="primary"
              size="large"
              type="submit"
            >
              Add
            </Button>
          </div>
        </form>
      </Paper>
    </Modal>
  );
};

export default CreateModule;
