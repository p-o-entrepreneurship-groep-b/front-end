import React from "react";
import "./Modal.css";

interface ModalProps {
  show: boolean;
  modalClassname: string;
}

const Modal: React.FC<ModalProps> = ({ show, modalClassname, children }) => {
  const mainClassname = show
    ? "modal-outer display-block"
    : "modal-outer display-none";

  return (
    <div className={mainClassname}>
      <div className={modalClassname}>{children}</div>
    </div>
  );
};

export default Modal;
