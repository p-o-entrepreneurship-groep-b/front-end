import React from "react";

import NotificationDropdown from "./NotificationDropDown/NotificationDropdown";

import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import NotificationsActiveIcon from "@material-ui/icons/NotificationsActive";
import IconButton from "@material-ui/core/IconButton";
import Badge from "@material-ui/core/Badge";

import "./Header.css";
import { useStoreState } from "../../Store";

const Header: React.FC = (props) => {
  const notifications = useStoreState((state) => state.incident.notifications);
  const hubs = useStoreState((state) => state.hub.byId);
  const modules = useStoreState((state) => state.module.byId);

  const [show, setShow] = React.useState(false);

  const onNotificationsClicked = () => {
    if (notifications.length === 0) return;

    if (!show) setShow(true);
  };

  return (
    <div className="header">
      <div className="header-dropdown">
        <IconButton onClick={onNotificationsClicked}>
          <Badge
            badgeContent={notifications.length}
            color="secondary"
            showZero={false}
          >
            <NotificationsActiveIcon color="inherit" />
          </Badge>
        </IconButton>
        <div className="dropdown-content">
          <NotificationDropdown
            show={show}
            handleClose={() => setShow(false)}
            notifications={notifications}
            hubs={hubs}
            modules={modules}
          />
        </div>
      </div>
      <IconButton>
        <ExitToAppIcon />
      </IconButton>
    </div>
  );
};

export default Header;
