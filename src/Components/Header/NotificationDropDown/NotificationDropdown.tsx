import React from "react";
import "./NotificationDropdown.css";
import NotificationEntry from "./NotificationEntry/NotificationEntry";

import Paper from "@material-ui/core/Paper";
import { Hub, Incident } from "../../../Api";
import { Module } from "../../../Api";
import { ById } from "../../../Store/ById";
import useOutsideAlerter from "../../../Common/UseOutsideAlerter";

const MAX_NOTIFICATIONS = 5;

interface NotificationDropdownProps {
  show: boolean;
  notifications: Incident[];
  hubs: ById<Hub>;
  modules: ById<Module>;
  handleClose: Function;
}

const NotificationDropdown: React.FC<NotificationDropdownProps> = ({
  show,
  handleClose,
  notifications,
  hubs,
  modules,
}) => {
  const ref = React.useRef(null);

  useOutsideAlerter(ref, handleClose);

  const incidentToEntry = (incident: Incident) => {
    const hub = hubs[incident.hub];
    const module = modules[incident.module];

    if (hub === undefined || module === undefined) return null;

    return (
      <NotificationEntry
        incident={incident}
        hub={hub}
        module={module}
        key={incident.id}
      />
    );
  };

  const className = show ? "show" : "hide";

  return (
    <Paper ref={ref} className={"notification-wrapper " + className}>
      <div>
        <div className="notification-header">
          <h3>Notifications</h3>
        </div>
        <div className="notification-entries">
          {notifications
            .sort((l, r) => r.timestamp.getTime() - l.timestamp.getTime())
            .map(incidentToEntry)
            .slice(0, MAX_NOTIFICATIONS)}
        </div>
        <div className="notification-footer">
          <p>See all incidents</p>
        </div>
      </div>
    </Paper>
  );
};

export default NotificationDropdown;
