import React from "react";
import { Hub, Incident, Module } from "../../../../Api";
import "./NotificationEntry.css";

interface NotificationEntryProps {
  incident: Incident;
  hub: Hub;
  module: Module;
}

const MILLISECONDS_IN_MINUTE = 1000 * 60;

const NotificationEntry: React.FC<NotificationEntryProps> = ({
  incident,
  hub,
  module,
}) => {
  const diff = new Date().getTime() - incident.timestamp.getTime();
  const minutesPast = Math.floor(diff / MILLISECONDS_IN_MINUTE);

  return (
    <li className="entry">
      <div className="entry-description">
        <h4>{hub.locationName}</h4>
        <p>{atob(incident.payload)}</p>
      </div>
      <div className="entry-timestamp">
        <p>{minutesPast}m</p>
      </div>
    </li>
  );
};

export default NotificationEntry;
