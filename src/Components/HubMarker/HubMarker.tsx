import React from "react";

import SettingsInputAntennaIcon from "@material-ui/icons/SettingsInputAntenna";

import { Marker } from "react-map-gl";

interface HubMarkerProps {
  longitude: number;
  latitude: number;
  onClick?: Function;
}

const HubMarker: React.FC<HubMarkerProps> = ({
  longitude,
  latitude,
  onClick,
}) => {
  return (
    <Marker
      longitude={longitude}
      latitude={latitude}
      offsetLeft={-6.5}
      offsetTop={-9.2}
    >
      <SettingsInputAntennaIcon
        style={{ cursor: "pointer", fontSize: "small", color: "red" }}
        onClick={() => onClick !== undefined && onClick()}
      />
    </Marker>
  );
};

export default HubMarker;
