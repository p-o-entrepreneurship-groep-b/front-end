import { Thunk, Action, Computed, thunk, action, computed } from "easy-peasy";
import { ById } from "./ById";
import { Module, ModulesApi } from "../Api";

const API = new ModulesApi();

export interface ModulesModel {
  byId: ById<Module>;

  modules: Computed<ModulesModel, Module[]>;

  fetchModules: Thunk<ModulesModel>;
  addModule: Thunk<ModulesModel, Module>;

  fetchedModules: Action<ModulesModel, Module[]>;
  addedModule: Action<ModulesModel, Module>;
}

const modulesModel: ModulesModel = {
  byId: {},
  modules: computed((state) => Object.values(state.byId)),

  fetchModules: thunk(async (actions) => {
    const response = await API.modulesList();
    actions.fetchedModules(response.data);
  }),

  addModule: thunk(async (actions, module) => {
    await API.modulesCreate(module);
    actions.addedModule(module);
  }),

  fetchedModules: action((state, modules) => {
    state.byId = modules.reduce((acc: ById<Module>, curr: Module) => {
      acc[curr.id] = curr;
      return acc;
    }, {});
  }),

  addedModule: action((state, module) => {
    state.byId[module.id] = module;
  }),
};

export default modulesModel;
