import { createTypedHooks } from "easy-peasy";
import { SencityStore } from "./Store";

const {
  useStoreActions,
  useStoreState,
  useStoreDispatch,
  useStore
} = createTypedHooks<SencityStore>();

export { useStoreActions, useStoreState, useStoreDispatch, useStore };
