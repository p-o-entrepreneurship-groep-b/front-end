import hubsModel, { HubsModel } from "./HubsModel";
import modulesModel, { ModulesModel } from "./ModulesModel";
import incidentsModel, { IncidentsModel } from "./IncidentsModel";

export interface SencityStore {
  hub: HubsModel;
  module: ModulesModel;
  incident: IncidentsModel;
}

const sencityStore: SencityStore = {
  hub: hubsModel,
  module: modulesModel,
  incident: incidentsModel,
};

export default sencityStore;
