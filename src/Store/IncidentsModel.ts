import { IncidentsApi, Incident } from "../Api";
import { ById } from "./ById";
import { Computed, Thunk, Action, computed, thunk, action } from "easy-peasy";

const API = new IncidentsApi();

export interface IncidentsModel {
  byId: ById<Incident>;

  lastSeen: Date;

  notifications: Computed<IncidentsModel, Incident[]>;
  incidents: Computed<IncidentsModel, Incident[]>;

  fetchIncidents: Thunk<IncidentsModel>;

  fetchedIncidents: Action<IncidentsModel, Incident[]>;
  markNotificationsAsSeen: Action<IncidentsModel>;
}

const incidentsModel: IncidentsModel = {
  byId: {},

  lastSeen: new Date(),

  notifications: computed((state) =>
    Object.values(state.byId).filter(
      (i) => i.timestamp.getTime() > state.lastSeen.getTime()
    )
  ),
  incidents: computed((state) => Object.values(state.byId)),

  fetchIncidents: thunk(async (state) => {
    const response = await API.incidentsList();
    state.fetchedIncidents(response.data);
  }),

  fetchedIncidents: action((state, incidents) => {
    state.byId = incidents.reduce((acc: ById<Incident>, curr: Incident) => {
      curr.timestamp = new Date(curr.timestamp);
      acc[curr.id] = curr;
      return acc;
    }, {});
  }),

  markNotificationsAsSeen: action((state) => {
    state.lastSeen = new Date();
  }),
};

export default incidentsModel;
