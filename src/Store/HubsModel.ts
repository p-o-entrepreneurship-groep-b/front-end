import { ById } from "./ById";
import { Hub, HubsApi, Module } from "../Api";
import { action, thunk, computed, Action, Thunk, Computed } from "easy-peasy";

const HUBS_API = new HubsApi();

type HubModule = [Hub, number];

export interface HubsModel {
  byId: ById<Hub>;
  hubs: Computed<HubsModel, Hub[]>;

  fetchHubs: Thunk<HubsModel>;

  addHub: Thunk<HubsModel, Hub>;
  editHub: Thunk<HubsModel, Hub>;
  deleteHub: Thunk<HubsModel, number>;

  addModuleToHub: Thunk<HubsModel, HubModule>;
  removeModuleFromHub: Thunk<HubsModel, HubModule>;

  fetchedHubs: Action<HubsModel, Hub[]>;

  setHub: Action<HubsModel, Hub>;
  deletedHub: Action<HubsModel, number>;
}

const hubsModel: HubsModel = {
  byId: {},

  hubs: computed((state) => Object.values(state.byId)),

  //Thunks
  fetchHubs: thunk(async (actions) => {
    const response = await HUBS_API.hubsList();
    actions.fetchedHubs(response.data);
  }),

  addHub: thunk(async (actions, hub) => {
    const response = await HUBS_API.hubsCreate(hub);
    actions.setHub(response.data);
  }),

  editHub: thunk(async (actions, hub) => {
    const response = await HUBS_API.hubsUpdate(hub.id, hub);
    actions.setHub(response.data);
  }),

  deleteHub: thunk(async (actions, id) => {
    await HUBS_API.hubsDelete(id);
    actions.deletedHub(id);
  }),

  addModuleToHub: thunk(async (actions, payload) => {
    const response = await HUBS_API.hubsModulesUpdate(
      payload[0].id,
      payload[1]
    );
    actions.setHub(response.data);
  }),

  removeModuleFromHub: thunk(async (actions, payload) => {
    const response = await HUBS_API.hubsModulesDelete(
      payload[0].id,
      payload[1]
    );
    actions.setHub(response.data);
  }),

  //Actions
  fetchedHubs: action((state, hubs) => {
    state.byId = hubs.reduce((acc: ById<Hub>, curr: Hub) => {
      acc[curr.id] = curr;
      return acc;
    }, {});
  }),

  setHub: action((state, hub) => {
    state.byId[hub.id] = hub;
  }),

  deletedHub: action((state, id) => {
    delete state.byId[id];
  }),
};

export default hubsModel;
