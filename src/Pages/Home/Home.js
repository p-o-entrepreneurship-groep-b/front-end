import React, { Fragment } from "react";
import Map from "./Map";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import { useStoreState } from "../../Store";

import HubMap from "../../Components/HubMap/HubMap";
import HubMarker from "../../Components/HubMarker/HubMarker";

const Home = () => {
  const hubs = useStoreState((state) => state.hub.hubs);

  return (
    <div>
      <HubMap height={900}>
        {hubs.map((h) => (
          <HubMarker longitude={h.longitude} latitude={h.latitude} />
        ))}
      </HubMap>
    </div>
  );
};

export default Home;
