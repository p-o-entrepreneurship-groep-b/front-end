import React, { Component, useState } from "react";
import MapGL, { GeolocateControl, Layer, Marker } from "react-map-gl";
import SettingsInputAntennaIcon from "@material-ui/icons/SettingsInputAntenna";
import { TextField } from "@material-ui/core";
import { Link } from "react-router-dom";

const MAPBOX_TOKEN =
  "pk.eyJ1IjoibHVpZ2lpaWlpaWkiLCJhIjoiY2s3dzQ4N2s0MDBmODNscXZ0MWRicjVzYyJ9.22f8kmcROhG5iUmhwJ9MHg";

const geolocateStyle = {
  float: "left",
  margin: "50px",
  padding: "10px",
};

const navigateStyle = {
  float: "right",
  // margin: '50px',
  padding: "10px",
};

const ICON_MAPPING = {
  marker: { x: 0, y: 0, width: 32, height: 32, mask: true },
};

const Map = () => {
  const [viewport, setViewport] = useState({
    width: 400,
    height: 400,
    latitude: 50.87912,
    longitude: 4.70128,
    zoom: 13,
  });

  const [pinpoints, setPinpoints] = useState([
    { id: 1, latitude: 50.87912, longitude: 4.70128, guid: 12345 },
    { id: 2, latitude: 50.878685, longitude: 4.700714, guid: 12346 },
  ]);

  const mapRef = React.createRef();

  const handleViewportChange = (viewport) => {
    setViewport(viewport);
  };

  const handleMouseClick = () => {
    console.log("click");
  };

  return (
    <div>
      <MapGL
        ref={mapRef}
        {...viewport}
        onViewportChange={handleViewportChange}
        width="50vw"
        height="80vh"
        mapStyle="mapbox://styles/luigiiiiiii/ck7w4b3q200781jqrruknh2ui"
        mapboxApiAccessToken={MAPBOX_TOKEN}
      >
        {pinpoints.map((data) => (
          <Link to={`/sensorStats/${data.guid}`}>
            <Marker
              key={data.id}
              name={data.id}
              latitude={data.latitude}
              longitude={data.longitude}
              offsetLeft={-6.5}
              offsetTop={-9.2}
            >
              <SettingsInputAntennaIcon
                style={{ cursor: "pointer", fontSize: "small", color: "red" }}
                onClick={() => {
                  console.log("click");
                }}
              />
            </Marker>
          </Link>
        ))}
      </MapGL>
    </div>
  );
};

export default Map;
