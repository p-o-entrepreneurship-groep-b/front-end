
import React from "react";
import HeatmapPng from "./heatmap.png"
import "./heatmap.css"
import Paper from '@material-ui/core/Paper';


const Heatmap = () => {
  return (
    <div class="outer">
    <div class="middle">
    <div class="inner">
      <Paper elevation={3}>
    <img src={HeatmapPng} className="photo"></img>
      </Paper>
    </div>
  </div>
</div>

  )
}

export default Heatmap;
