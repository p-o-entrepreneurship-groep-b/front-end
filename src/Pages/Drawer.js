import React, { Fragment } from "react";
import Drawer from "@material-ui/core/Drawer";
import clsx from "clsx";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import MailIcon from "@material-ui/icons/Mail";
import styled from "styled-components";
import MapIcon from "@material-ui/icons/Map";
import { Link } from "react-router-dom";

const HoverText = styled.p`
  color: #000;
  :hover {
    color: #ed1212;
    cursor: pointer;
  }
`;
const drawerWidthText = 240;

const useStyles = makeStyles((theme) => ({
  div: {
    width: theme.spacing(8),
  },
  hide: {
    display: "none",
  },
  drawer: {
    width: drawerWidthText,
    flexShrink: 0,
    whiteSpace: "nowrap",
    backgroundColor: "#222B3C",
  },
  drawerOpen: {
    width: drawerWidthText,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: "hidden",
    width: theme.spacing(8),
  },
  toolbar: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  textcol: {
    color: "#919EAF",
  },
  divider: {
    color: "#FFFFFF",
    backgroundColor: "#919EAF",
  },
}));

const DrawerElem = () => {
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <div
      className={classes.div}
      onMouseEnter={handleDrawerOpen}
      onMouseLeave={handleDrawerClose}
    >
      <Drawer
        variant="permanent"
        className={classes.drawer}
        className={clsx({
          [classes.drawerOpen]: open,
          [classes.drawerClose]: open,
        })}
        classes={{
          paper: clsx(classes.drawer, {
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          }),
        }}
      >
        <List>
          {["Map", "Starred", "Send email", "Drafts"].map((text, index) => (
            <Link to="/">
              <ListItem button key={text}>
                <ListItemIcon className={classes.textcol}>
                  {index % 2 === 0 ? <MapIcon /> : <MailIcon />}
                </ListItemIcon>
                <ListItemText className={classes.textcol} primary={text} />
              </ListItem>
            </Link>
          ))}
        </List>
      </Drawer>
    </div>
  );
};

export default DrawerElem;
