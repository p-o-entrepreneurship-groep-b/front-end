import React from "react";

import "../../../App.css";
import { Module, Hub } from "../../../Api";
import { useStoreState, useStoreActions } from "../../../Store";

import { useForm, Controller } from "react-hook-form";

import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import Modal from "../../../Components/Modal/Modal";
import FormControl from "@material-ui/core/FormControl";
import FormHelperText from "@material-ui/core/FormHelperText";
import NativeSelect from "@material-ui/core/NativeSelect";

interface AddModuleProps {
  hub: Hub;
  show: boolean;
  handleClose: Function;
}

interface FormData {
  moduleId: number;
}

const AddModule: React.FC<AddModuleProps> = ({ hub, show, handleClose }) => {
  const { handleSubmit, errors, reset, control } = useForm<FormData>({
    mode: "onBlur",
  });

  const modules = useStoreState((state) => state.module.modules);

  const addModuleToHub = useStoreActions((state) => state.hub.addModuleToHub);

  const onSubmit = (data: FormData) => {
    console.log(data.moduleId);
    addModuleToHub([hub, data.moduleId]);
    reset();
    handleClose();
  };

  return (
    <Modal show={show} modalClassname="modal-outer-form">
      <Paper className="modal-form-surface">
        <form onSubmit={handleSubmit(onSubmit)}>
          <h2>Add module</h2>
          <div className="modal-form-item">
            <FormControl error={errors.moduleId !== undefined} fullWidth={true}>
              <Controller
                name="moduleId"
                as={
                  <Select fullWidth={true}>
                    {modules.map((m) => (
                      <MenuItem key={m.id} value={m.id}>
                        {m.name}
                      </MenuItem>
                    ))}
                  </Select>
                }
                control={control}
                rules={{
                  required: "You must select a module",
                  validate: (value: number) =>
                    !hub.modules.includes(value) ||
                    "This module is already installed",
                }}
                defaultValue=""
              />
              {errors.moduleId !== undefined && (
                <FormHelperText>{errors.moduleId.message}</FormHelperText>
              )}
            </FormControl>
          </div>
          <div className="modal-form-items modal-form-controls">
            <Button
              variant="contained"
              color="secondary"
              size="large"
              onClick={() => {
                reset();
                handleClose();
              }}
            >
              Cancel
            </Button>
            <Button
              variant="contained"
              color="primary"
              size="large"
              type="submit"
            >
              Add
            </Button>
          </div>
        </form>
      </Paper>
    </Modal>
  );
};

export default AddModule;
