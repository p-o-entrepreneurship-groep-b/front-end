import React from "react";
import { useParams, Link } from "react-router-dom";

import "./Hubs.css";

import ModuleListItem from "./ModuleListItem/ModuleListItem";
import { Hub } from "../../Api";
import { useStoreState, useStoreActions } from "../../Store";
import CreateHub from "./CreateHub/CreateHub";
import HubDetails from "./HubDetails/HubDetails";
import AddModule from "./AddModule/AddModule";

import Checkbox from "@material-ui/core/Checkbox";
import CircleCheckedFilled from "@material-ui/icons/CheckCircle";
import CircleUnchecked from "@material-ui/icons/RadioButtonUnchecked";

import TableContainer from "@material-ui/core/TableContainer";
import TableRow from "@material-ui/core/TableRow";
import TableHead from "@material-ui/core/TableHead";
import TableCell from "@material-ui/core/TableCell";
import Paper from "@material-ui/core/Paper";
import HubMap from "../../Components/HubMap/HubMap";
import List from "@material-ui/core/List";

import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import IconButton from "@material-ui/core/IconButton";
import AddBoxIcon from "@material-ui/icons/AddBox";
import HubMarker from "../../Components/HubMarker/HubMarker";

const Hubs: React.FC<{}> = () => {
  const hubs = useStoreState((state) => state.hub.hubs);
  const modulesById = useStoreState((state) => state.module.byId);
  const incidents = useStoreState((state) => state.incident.incidents);

  const removeModuleFromHub = useStoreActions(
    (state) => state.hub.removeModuleFromHub
  );

  const [showCreateHub, setShowCreateHub] = React.useState(false);
  const [showAddModule, setShowAddModule] = React.useState(false);

  const { guid } = useParams();
  const selectedHub = hubs.find((h) => h.guid === guid);

  const hubToTableRow = (hub: Hub) => {
    const selected = selectedHub !== undefined && selectedHub.id === hub.id;

    return (
      <TableRow key={hub.id}>
        <TableCell>
          <Link to={"/hubs/" + hub.guid}>
            <Checkbox
              icon={<CircleUnchecked />}
              checkedIcon={<CircleCheckedFilled />}
              checked={selected}
            />
          </Link>
        </TableCell>
        <TableCell className="hubs-table-location">
          {hub.locationName}
        </TableCell>
        <TableCell>{hub.guid}</TableCell>
      </TableRow>
    );
  };

  return (
    <div className="hubs-wrapper">
      <div className="hubdetail-content">
        <Paper className="hubdetail-map">
          <HubMap
            height={500}
            width={500}
            fixed={true}
            zoom={selectedHub ? 17 : undefined}
            longitude={selectedHub?.longitude}
            latitude={selectedHub?.latitude}
          >
            {hubs.map((h) => (
              <HubMarker
                key={h.id}
                longitude={h.longitude}
                latitude={h.latitude}
              />
            ))}
          </HubMap>
        </Paper>
        <div className="hubdetail-info">
          <HubDetails
            hub={selectedHub}
            incidents={
              selectedHub == null
                ? []
                : incidents.filter((i) => i.hub === selectedHub.id)
            }
          />
          <Paper className="hubdetail-info-section hubdetail-modules">
            <div className="hubdetail-modules-toolbar">
              {selectedHub !== undefined && (
                <AddModule
                  hub={selectedHub}
                  show={showAddModule}
                  handleClose={() => setShowAddModule(false)}
                />
              )}
              <h3>Enabled modules</h3>
              {selectedHub !== undefined && (
                <IconButton size="small" onClick={() => setShowAddModule(true)}>
                  <AddBoxIcon />
                </IconButton>
              )}
            </div>
            <List>
              {selectedHub !== undefined
                ? selectedHub.modules.map((id) => (
                    <ModuleListItem
                      key={id}
                      module={modulesById[id]}
                      handleDeleteClicked={() =>
                        removeModuleFromHub([selectedHub, id])
                      }
                    />
                  ))
                : null}
            </List>
          </Paper>
        </div>
      </div>
      <div className="hubs-table hubs-table-container">
        <CreateHub
          show={showCreateHub}
          handleClose={() => setShowCreateHub(false)}
        ></CreateHub>
        <Paper className="hubs-table">
          <Toolbar className="hubs-table-toolbar">
            <Typography className="hubs-table-title" variant="h6">
              Hubs
            </Typography>
            <IconButton onClick={() => setShowCreateHub(true)} size="medium">
              <AddBoxIcon />
            </IconButton>
          </Toolbar>
          <TableContainer>
            <Table stickyHeader>
              <TableHead className="hubs-table-head">
                <TableRow>
                  <TableCell />
                  <TableCell className="hubs-table-location">
                    Location name
                  </TableCell>
                  <TableCell>Guid</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>{hubs.map(hubToTableRow)}</TableBody>
            </Table>
          </TableContainer>
        </Paper>
      </div>
    </div>
  );
};

export default Hubs;
