import React from "react";

import "../../../App.css";
import "./CreateHub.css";
import Modal from "../../../Components/Modal/Modal";
import { useForceUpdate } from "../../../Common/Hooks";

import { useForm } from "react-hook-form";

import { Hub } from "../../../Api";
import { useStoreState, useStoreActions } from "../../../Store";
import HubMap from "../../../Components/HubMap/HubMap";
import { PointerEvent } from "react-map-gl";

import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

import HubMarker from "../../../Components/HubMarker/HubMarker";

const GUID_PATTERN = /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i;

interface CreateHubProps {
  show: boolean;
  handleClose: Function;
}

const CreateHub: React.FC<CreateHubProps> = ({ show, handleClose }) => {
  const {
    register,
    handleSubmit,
    errors,
    reset,
    getValues,
    setValue,
  } = useForm<Hub>({
    mode: "onBlur",
  });

  const hubs = useStoreState((state) => state.hub.hubs);
  const addHub = useStoreActions((state) => state.hub.addHub);

  const forceUpdate = useForceUpdate();

  const isGuidFree = (guid: string) =>
    hubs.filter((h) => h.guid === guid).length === 0;

  const isLocationFree = (longitude: number, latitude: number) =>
    hubs.filter((h) => h.longitude === longitude && h.latitude === latitude)
      .length === 0;

  const isLocationNameFree = (locationName: string) =>
    hubs.filter((h) => h.locationName === locationName).length === 0;

  const renderMarker = () => {
    if (getValues().longitude === undefined) return null;

    return (
      <HubMarker
        longitude={getValues().longitude}
        latitude={getValues().latitude}
      />
    );
  };

  register(
    {
      name: "longitude",
      type: "custom",
    },
    {
      required: "You must select a location",
      validate: (longitude) => {
        const latitude = getValues().latitude;
        return isLocationFree(longitude, latitude);
      },
    }
  );
  register({
    name: "latitude",
    type: "custom",
  });

  const locationClicked = (event: PointerEvent) => {
    setValue("longitude", event.lngLat[0]);
    setValue("latitude", event.lngLat[1]);
    forceUpdate();
  };

  const onSubmit = (data: Hub) => {
    addHub(data);
    handleClose();
  };

  return (
    <Modal show={show} modalClassname="modal-outer-form">
      <Paper className="modal-form-surface">
        <form onSubmit={handleSubmit(onSubmit)}>
          <h2>Create hub</h2>
          <div className="modal-form-item">
            <TextField
              name="guid"
              label="Guid"
              fullWidth={true}
              variant="filled"
              type="text"
              inputRef={register({
                required: "You must enter a guid",
                pattern: {
                  value: GUID_PATTERN,
                  message: "You must enter a valid guid",
                },
                validate: (value) =>
                  isGuidFree(value) ||
                  "A hub with the given guid already exists",
              })}
              error={errors.guid !== undefined}
              helperText={errors.guid && errors.guid.message}
            />
          </div>
          <div className="modal-form-item">
            <TextField
              name="locationName"
              label="Location name"
              fullWidth={true}
              variant="filled"
              type="text"
              inputRef={register({
                required: "You must enter a location name",
                minLength: {
                  value: 5,
                  message:
                    "The location name must be at least 5 characters long",
                },
                validate: (value) =>
                  isLocationNameFree(value) ||
                  "A hub with that location name already exists",
              })}
              error={errors.locationName !== undefined}
              helperText={errors.locationName && errors.locationName.message}
            />
          </div>
          <div className="modal-form-item modal-form-map">
            <h4>Location</h4>
            {errors.longitude ? <p>{errors.longitude.message}</p> : null}
            <HubMap height={300} onClick={locationClicked}>
              {renderMarker()}
            </HubMap>
          </div>

          <div className="modal-form-item modal-form-controls">
            <Button
              variant="contained"
              color="secondary"
              size="large"
              onClick={() => {
                reset();
                handleClose();
              }}
            >
              Cancel
            </Button>
            <Button
              variant="contained"
              color="primary"
              size="large"
              type="submit"
            >
              Add
            </Button>
          </div>
        </form>
      </Paper>
    </Modal>
  );
};

export default CreateHub;
