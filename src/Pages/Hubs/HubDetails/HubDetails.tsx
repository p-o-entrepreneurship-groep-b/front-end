import React from "react";

import { Hub, Incident } from "../../../Api";

import Paper from "@material-ui/core/Paper";

interface HubDetailsProps {
  hub: Hub | undefined;
  incidents: Incident[];
}

const MILLISECONDS_IN_HOUR = 1000 * 60 * 60;

const HubDetails: React.FC<HubDetailsProps> = ({ hub, incidents }) => {
  const now = new Date();

  const incidentsLastHour = incidents.filter(
    (i) => now.getTime() - i.timestamp.getTime() < MILLISECONDS_IN_HOUR
  ).length;

  const incidentsToday = incidents.filter(
    (i) =>
      i.timestamp.getFullYear() === now.getFullYear() &&
      i.timestamp.getMonth() === now.getMonth() &&
      i.timestamp.getDay() === now.getDay()
  ).length;

  return (
    <Paper className="hubdetail-info-section">
      <h3>Hub details</h3>
      <p>Location name: {hub?.locationName}</p>
      <p>Guid: {hub?.guid}</p>
      <p>Incidents in the last hour: {" " + incidentsLastHour}</p>
      <p>Incidents today: {" " + incidentsToday}</p>
    </Paper>
  );
};

export default HubDetails;
