import React from "react";
import { Module } from "../../../Api";

import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";

interface ModuleListItemProps {
  module: Module;
  handleDeleteClicked: Function;
}

const ModuleListItem: React.FC<ModuleListItemProps> = ({
  module,
  handleDeleteClicked,
}) => {
  return (
    <ListItem divider={true}>
      <ListItemText primary={module.name} />
      <ListItemSecondaryAction>
        <IconButton onClick={() => handleDeleteClicked()}>
          <DeleteIcon />
        </IconButton>
      </ListItemSecondaryAction>
    </ListItem>
  );
};

export default ModuleListItem;
