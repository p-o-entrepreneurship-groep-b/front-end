import React, { Component, useState } from "react";
import Module from "./Module";

const ActiveModules = () => {
  const [hub, setHub] = useState({
    id: 12345,
    location_name: "Grote Markt",
    longitude: 4.70128,
    latitude: 50.87912,
    identifier: 696969,
  });

  const [activeModules, setActiveModules] = useState([
    { id: 15, name: "Thermometer" },
    { id: 16, name: "CO2-sensor" },
  ]);

  return (
    <div>
      <h2>Active Modules</h2>
      <ul>
        {activeModules.map((module) => (
          <Module name={module.name} />
        ))}
      </ul>
    </div>
  );
};

export default ActiveModules;
