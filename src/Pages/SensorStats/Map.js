import React, { Component, useState } from "react";
import MapGL, { GeolocateControl, Layer, Marker } from "react-map-gl";
import SettingsInputAntennaIcon from "@material-ui/icons/SettingsInputAntenna";
import { TextField } from "@material-ui/core";
import { Link } from "react-router-dom";

const MAPBOX_TOKEN =
  "pk.eyJ1IjoibHVpZ2lpaWlpaWkiLCJhIjoiY2s3dzQ4N2s0MDBmODNscXZ0MWRicjVzYyJ9.22f8kmcROhG5iUmhwJ9MHg";

const geolocateStyle = {
  float: "left",
  margin: "50px",
  padding: "10px",
};

const navigateStyle = {
  float: "right",
  // margin: '50px',
  padding: "10px",
};

const ICON_MAPPING = {
  marker: { x: 0, y: 0, width: 32, height: 32, mask: true },
};

const Map = () => {
  const [pinpoint, setPinpoint] = useState({
    id: 1,
    latitude: 50.87912,
    longitude: 4.70128,
    guid: 12345,
  });

  const [viewport, setViewport] = useState({
    longitude: pinpoint.longitude,
    latitude: pinpoint.latitude,
    zoom: 15,
  });

  const mapRef = React.createRef();

  return (
    <div>
      <MapGL
        ref={mapRef}
        {...viewport}
        width="150px"
        height="200px"
        mapStyle="mapbox://styles/luigiiiiiii/ck7w4b3q200781jqrruknh2ui"
        mapboxApiAccessToken={MAPBOX_TOKEN}
      >
        <Marker
          key={pinpoint.id}
          name={pinpoint.id}
          latitude={pinpoint.latitude}
          longitude={pinpoint.longitude}
          offsetLeft={-6.5}
          offsetTop={-9.2}
        >
          <SettingsInputAntennaIcon
            style={{ cursor: "pointer", fontSize: "small", color: "green" }}
            onClick={() => {
              console.log("click");
            }}
          />
        </Marker>
      </MapGL>
    </div>
  );
};

export default Map;
