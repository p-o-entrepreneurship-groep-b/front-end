import React, { Component, useState } from "react";
import Info from "./Info";
import Map from "./Map";
import Incidents from "./Incidents";
import ActiveModules from "./ActiveModules";

const SensorStats = () => {
  const [hub, setHub] = useState({
    id: 12345,
    location_name: "Grote Markt",
    longitude: 4.70128,
    latitude: 50.87912,
    identifier: 696969,
  });

  return (
    <div>
      <h1>This is the SensorPage</h1>
      <Map />
      <Info />
      <Incidents />
      <ActiveModules />
    </div>
  );
};

export default SensorStats;
