import React, { Component, useState } from "react";

const SensorInfo = () => {
  const [hub, setHub] = useState({
    id: 12345,
    location_name: "Grote Markt",
    longitude: 4.70128,
    latitude: 50.87912,
    identifier: 696969,
  });

  return (
    <div>
      <h2>Info</h2>
      <ul>
        <li>id: {hub.identifier}</li>
        <li>location: {hub.location_name}</li>
      </ul>
    </div>
  );
};

export default SensorInfo;
