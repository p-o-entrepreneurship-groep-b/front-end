import React, { Component, useState } from "react";
import Incident from "./Incident";

const Incidents = () => {
  const [hub, setHub] = useState({
    id: 12345,
    location_name: "Grote Markt",
    longitude: 4.70128,
    latitude: 50.87912,
    identifier: 696969,
  });

  const [incidents, setIncidents] = useState([
    { id: 69, hub: 12345, time: 15, module_id: 420, payload: 125 },
    { id: 70, hub: 12345, time: 16, module_id: 666, payload: 126 },
    { id: 73, hub: 12345, time: 17, module_id: 101, payload: 1045 },
  ]);

  return (
    <div>
      <h2>Incidents</h2>
      <ul>
        {incidents.map((incident) => (
          <Incident
            hub={incident.hub}
            time={incident.time}
            module_id={incident.module_id}
            payload={incident.payload}
          />
        ))}
      </ul>
    </div>
  );
};

export default Incidents;
