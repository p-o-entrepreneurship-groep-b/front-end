import React, { Component, useState } from "react";

const Incident = (props) => {
  return (
    <li>
      hub: {props.hub}, time: {props.time}, module: {props.module_id}, payload:{" "}
      {props.payload}
    </li>
  );
};

export default Incident;
