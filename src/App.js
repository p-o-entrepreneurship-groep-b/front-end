import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";

import { StoreProvider } from "easy-peasy";
import { createStore } from "easy-peasy";

import HomeIcon from "@material-ui/icons/Home";
import DeviceHubIcon from "@material-ui/icons/DeviceHub";
import SettingsInputAntennaIcon from "@material-ui/icons/SettingsInputAntenna";
import NotificationsIcon from "@material-ui/icons/Notifications";
import DataIcon from "@material-ui/icons/Assessment";

import "./App.css";

import storeModel from "./Store/Store";
import Sidebar from "./Components/Sidebar/Sidebar";
import Header from "./Components/Header/Header";
import Home from "./Pages/Home/Home";
import SensorStats from "./Pages/SensorStats/SensorStats";
import Notifications from "./Pages/Notifications/Notifications";
import Hubs from "./Pages/Hubs/Hubs";
import { useInterval } from "./Common/UseInterval";
import { useStoreActions } from "./Store";
import Heatmap from "./Pages/Heatmap/Heatmap";

const ROUTES = [
  {
    label: "Home",
    exact: true,
    path: "/",
    icon: <HomeIcon />,
    sidebar: true,
    component: Home,
  },
  {
    label: "Hubs",
    path: "/hubs/:guid?",
    icon: <DeviceHubIcon />,
    sidebar: true,
    component: Hubs,
  },
  {
    label: "SensorStats",
    path: "/sensorStats/:guid",
    icon: <SettingsInputAntennaIcon />,
    sidebar: false,
    component: SensorStats,
  },
  {
    label: "Notifications",
    path: "/notifications",
    icon: <NotificationsIcon />,
    sidebar: true,
    component: Notifications,
  },
  {
    label: "Data analysis",
    path: "/heatmap",
    icon: <DataIcon />,
    sidebar: true,
    component: Heatmap,
  },
];

function App() {
  const fetchIncidents = useStoreActions(
    (state) => state.incident.fetchIncidents
  );
  const fetchHubs = useStoreActions((state) => state.hub.fetchHubs);
  const fetchModules = useStoreActions((state) => state.module.fetchModules);

  useInterval(() => {
    fetchIncidents();
  }, 3000);

  React.useEffect(() => {
    fetchModules();

    fetchHubs();
  }, [fetchHubs, fetchModules]);

  return (
    <React.Fragment>
      <BrowserRouter>
        <div className="sidebar-wrapper">
          <Sidebar entries={ROUTES.filter((r) => r.sidebar)} />
        </div>
        <div className="header-wrapper">
          <Header />
        </div>
        <div className="content">
          <Switch>
            {ROUTES.map((r) => (
              <Route
                key={r.path}
                path={r.path}
                component={r.component}
                exact={r.exact}
              />
            ))}
          </Switch>
        </div>
      </BrowserRouter>
    </React.Fragment>
  );
}

export default App;
