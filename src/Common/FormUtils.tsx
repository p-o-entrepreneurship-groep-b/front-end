
export const transformFloat = ([event] : any[]) => {
    const number = parseFloat(event.target.value);
    return isNaN(number) ? undefined : number;
}