import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { createStore, StoreProvider } from "easy-peasy";
import storeModel from "./Store/Store";

const store = createStore(storeModel);

ReactDOM.render(
  <StoreProvider store={store}>
    <App />
  </StoreProvider>,
  document.getElementById("root")
);
